package com.arun.aashu.assignmentcustomdialog;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final TextView t = findViewById(R.id.name);


        final Dialog dialog = new Dialog(this);

        dialog.setContentView(R.layout.custom_dialog);
        dialog.setCancelable(true);
        dialog.show();


        (dialog.findViewById(R.id.login)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                EditText e1 = dialog.findViewById(R.id.user);



                String name = e1.getText().toString();


                if (name.length() >3) {

                    t.setText(name);
                    dialog.dismiss();

                } else {
                    e1.setError("Name should more then three letters");

                }


            }
        });


    }
}
